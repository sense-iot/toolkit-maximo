import pandas as pd
import xmltodict
import json
import requests
import os
import sys
from config import maxauth

#add the html to the path so we can import the assetnum python file
sys.path.insert(1, '/var/www/html')
from maximoconfig import maximoassetnum

# Maximo API key in Base64 format
api_key = maxauth
asset_num = maximoassetnum
# Params for setting up base URL
obj_struct = 'SNDASSET2'

#change working directory so the dataset ends up in the correct place
os.chdir("/var/www/html/")

def get_response() :
    """This function launches the GET request to Maximo"""
    url = 'https://maximotest.tbi.nl/maximo/rest/os/' + obj_struct + '?'
    headers = {
        'maxauth': api_key,
        'Accept-Language': 'application/xml'
    }
    url_params = {  # parameters passed to the API
        'assetnum': asset_num,
    }

    response = requests.get(url, headers=headers, params=url_params)

    if response.status_code == 200:
        print('Success!')
    elif response.status_code == 404:
        print('ApiError')

    return response


def decode_response(response) :
    """This code decodes the response, parses it from XML to JSON and filters the Base64 formatted
     pictures in a Pandas DataFrame"""
    # Decode the response
    decoded_response = response.content.decode('utf-8')
    # Convert to .JSON object
    response_json = json.loads(json.dumps(xmltodict.parse(decoded_response)))
    # Read into Pandas's DataFrame
    data_set = pd.DataFrame(response_json)
    # Filter data on QueryGMB_TESTResponse row[8]
    data = data_set.iloc[8]
    # Convert DataFrame to new indented .JSON object
    new_data = json.dumps(dict(data), indent=4)  # use dict and not list because of the .JSON format

    return new_data


def response_todoc(file_name) :
    """Opens a document and writes the values from the GET request to the document"""
    f = open(file_name + '.json', 'w+')
    global new_data
    f.write(new_data)
    f.close()


response = get_response()
new_data = decode_response(response)
response_todoc('dataset')
